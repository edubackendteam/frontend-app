import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { ContainerComponent } from './container/container.component';
import { Shallow } from 'shallow-render';

describe('AppComponent', () => {

  let shallow: Shallow<AppComponent>;
  const title = 'Welcome to app!';
  beforeEach(() => {
    shallow = new Shallow(AppComponent, null);
  });

  it('Should create component', async () => {
    const { fixture } = await shallow.render();
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'app'`, async () => {
    const { fixture } = await shallow.render();

    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  });

  it('should render title in a h1 tag', async () => {
    const { fixture } = await shallow.render();
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain(title);
  });
});



