
import { Component, Input, Output, EventEmitter } from '@angular/core';



@Component({
    selector: 'app-button',
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.scss']
})

export class ButtonComponent {
    @Input() text: string;
    @Output() buttonClick = new EventEmitter<Event>();
    constructor(
    ) {
    }
    onButtonClick(event: Event) {
        this.buttonClick.emit(event);
        console.log('clicked');
    }
}
