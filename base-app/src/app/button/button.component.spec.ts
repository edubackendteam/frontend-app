import { ButtonComponent } from './button.component';

import { Shallow } from 'shallow-render';
import { AppComponent } from './../app.component';

describe('ButtonComponent', () => {
    let shallow: Shallow<ButtonComponent>;
    const buttonLabel = 'Test Button Name';
    const buttonTemplate = `<app-button [text]="buttonLabel" (buttonClick)="onButtonClick($event)"></app-button>`;
    const render = (bindings: any = {
        text: buttonLabel
    }) => {
        return shallow.render(buttonTemplate, { bind: bindings });
    };

    beforeEach(() => {
        shallow = new Shallow(ButtonComponent, AppComponent);
    });

    it('Should create component', async () => {
        console.log(111111);
        const { fixture } = await render();
        console.log(22222);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeFalsy();
    });
    // it('Should match button label name', async () => {
    //     const { fixture, find } = await render();
    //     fixture.detectChanges();
    //     const compiled = fixture.debugElement.nativeElement;
    //    // expect(compiled.querySelector('button').textContent).toContain('df');
    //     console.log(find('.error-heading'));
    //    expect(find('.error-heading')[0].nativeElement.innerText).toBe(`insights.RUNTIME_ERROR_MESSAGE.`);
    // });

});




/*import { of as observableOf } from 'rxjs';
import { Shallow } from 'shallow-render';
import { Mock } from 'ts-mockery';

import { InsightsReportOutputComponent } from './report-output.component';
import { ReportOutput, ReportExecution, Report } from '../../models/report.model';
import { InsightsModule } from '../../insights.module';
import { ListViewModule } from '@synerg/components/list-view';
import { ReportOutputService } from './report-output.service';
import { VirtualScrollComponent } from 'angular2-virtual-scroll';
import { ReportRunService } from '../../services/report-run.service';
import { ReportFormattingService } from '../../services/report-formatting.service';
import { ModalComponent } from '@espresso/modal';
import { LanguageService } from '@myadp/common';
import { FeedbackLinkComponent } from '@myadp/feedback';
import { DebugElement } from '@angular/core';

describe('InsightsReportOutputComponent', () => {
    let shallow: Shallow<InsightsReportOutputComponent>;
    let mockReportOutputData: ReportOutput;
    let mockReportExecutionData: ReportExecution;
    let reportOutputTemplate: string = `
        <insights-report-output [report]="currentSelectedReport" [executionId]="currentExecutionId"></insights-report-output>`;
    const report: Report = {
        'reportName': 'Pay Rate Report',
        'templateID': '46a4f7f2-26d1-4031-a28e-f6644744b21e',
        'executions': [
            {
                'executionId': '1818008570980',
                'executionDateTime': '2018-06-29T14:09:54.349Z',
                'rowCount': 10,
                'executionStatus': 'completed'
            }
        ]
    };

    const render = (bindings: any = {
        currentSelectedReport: report,
        currentExecutionId: report.executions[0].executionId
    }) => {
        return shallow.render(reportOutputTemplate, { bind: bindings });
    };

    mockReportOutputData = Mock.of<ReportOutput>({
        'executionId': '1816208470130',
        'reportTemplateId': '656161b1-f741-4d74-ba3d-ea8545adcc08',
        'dataColumns': [
        ],
        'rowCount': 2,
        'dataRows': [{
            'rowID': 1,
            'dataItems': [{
                'itemValue': 'Amy',
                'formattedValue': 'Amy'
            },
            {
                'itemValue': 'Petro',
                'formattedValue': 'Petro'
            },
            {
                'itemValue': '1950/10/4',
                'formattedValue': 'October 4, 1950'
            }]
        },
        {
            'rowID': 2,
            'dataItems': [{
                'itemValue': 'Mary',
                'formattedValue': 'Mary'
            },
            {
                'itemValue': 'Trivoli',
                'formattedValue': 'Trivoli'
            },
            {
                'itemValue': '1965/6/7',
                'formattedValue': 'June 7, 1965'
            }]
        }]
    });

    mockReportExecutionData = Mock.of<any>({
        'executionId': '1816208470130',
        'reportTemplateId': '656161b1-f741-4d74-ba3d-ea8545adcc08',
        'executionTitle': 'Check Details',
        'executionNote': 'Report on June, 2018 for Accounting Dept',
        'runtimeSettings': {
            'runtimeFilters': [{
                'sequenceNumber': 1,
                'filterField': {
                    'fieldID': 'JOB@@HRII@@153.DEPTID',
                    'fieldName': 'Department Code',
                    'dataTypeCode': 'STR',
                    'catalogFileName': 'Job Information'
                },
                'operatorCode': 'in',
                'filterValues': [{
                    'itemID': '10000',
                    'itemValue': '10000',
                    'sequenceNumber': 1,
                    'itemTitle': 'Executive'
                },
                {
                    'itemID': '10100',
                    'itemValue': '10100',
                    'sequenceNumber': 2,
                    'itemTitle': 'Accounting'
                }
                ]
            }
            ]
        },
        'executionDateTime': '2018-06-11T03:12:10.000Z',
        'rowCount': 24,
        'executionStatus': 'completed'
    });

    beforeEach(() => {
        shallow = new Shallow(InsightsReportOutputComponent, InsightsModule)
            .mock(ReportOutputService, {
                getReportOutput: () => observableOf(mockReportOutputData),
                getReportExecution: () => observableOf(mockReportExecutionData)
            })
            .mock(ReportRunService, {
                getReportRuntimeStatusListner: () => observableOf(null),
                getRuntimeSettingsListner: () => observableOf(null),
                runReport: (rpt: Report) => undefined
            })
            .mock(ReportFormattingService, {
                format: () => '08/23/2018 12:32:17 PM EST'
            })
            .mock(LanguageService, {
                get: () => 'EXPORT'
            })
            .dontMock(ListViewModule);
    });
    it('should create the component', async () => {
        const { instance } = await render();
        expect(instance).toBeTruthy();
    });

    it('should call loadReportExecutionData and loadReportOutputData', async () => {
        const { instance } = await render();
        Mock.extend(instance).with({
            loadReportExecutionData: Mock.noop,
            loadReportOutputData: Mock.noop
        });
        expect(instance.executionId).toBe(report.executions[0].executionId);
        expect(instance.reportTemplateId).toBe(report.templateID);
    });

    it('should have same rows', async () => {
        const { find, fixture, findComponent } = await render();
        findComponent(VirtualScrollComponent).viewPortItems = mockReportOutputData.dataRows;
        fixture.detectChanges();
        expect(find('tbody .table-row')[0].nativeElement.children.length).toBe(mockReportOutputData.dataRows[0].dataItems.length);
    });

    it('should show report execution data unavailable on error response', async () => {
        const { fixture, find, findComponent } = await render();
        findComponent(VirtualScrollComponent).viewPortItems = [];
        fixture.detectChanges();
        expect(find('tbody .table-row')).toHaveFound(0);
    });

    it('should match number of report outputs length', async () => {
        const { find, fixture, findComponent } = await render();
        findComponent(VirtualScrollComponent).viewPortItems = mockReportOutputData.dataRows;
        fixture.detectChanges();
        expect(find('tbody .table-row').length).toBe(mockReportOutputData.dataRows.length);
    });

    it('should set feedback pageId and title', async () => {
        const feedbackLabel = 'Give feedback';
        shallow.mock(LanguageService, {
            get: () => feedbackLabel
        });
        const { instance } = await render();
        Mock.extend(instance).with({
            loadReportExecutionData: Mock.noop,
            loadReportOutputData: Mock.noop
        });
        instance.setFeedbackDetails();

        let expectedTitle = `${feedbackLabel} for ${report.reportName}`;
        let expectedPageId = `${report.executions[0].executionId}_payRateReport_08_23_2018_12_32_17_PM`;

        expect(instance.feedbackTitle).toBe(expectedTitle);
        expect(instance.feedbackLabel).toBe(feedbackLabel);
        expect(instance.feedbackPageId.length).toBeLessThanOrEqual(50);
        expect(instance.feedbackPageId).toBe(expectedPageId);
    });

    it('should open export modal for expoting', async () => {
        const { instance, fixture, findComponent } = await render();
        const modalcompoent = findComponent(ModalComponent);
        modalcompoent.openModal = () => undefined;
        spyOn(modalcompoent, 'openModal');
        fixture.detectChanges();
        instance.openExportModal();
        expect(modalcompoent.openModal).toHaveBeenCalled();
    });

    it('should open the feedback modal', async () => {
        const { instance, fixture, findComponent } = await render();
        const modalcompoent = findComponent(FeedbackLinkComponent);
        modalcompoent.openFeedbackModal = () => undefined;
        spyOn(modalcompoent, 'openFeedbackModal');
        fixture.detectChanges();
        instance.openFeedback();
        expect(modalcompoent.openFeedbackModal).toHaveBeenCalled();
    });
});
*/
