
import { Component, Input, Inject } from '@angular/core';



@Component({
    selector: 'app-container',
    templateUrl: './container.component.html',
    styleUrls: ['./container.component.scss']
})

export class ContainerComponent {
    constructor(
    ) {
    }
    onButtonClick(event: Event) {
        console.log('got it');
    }
}
